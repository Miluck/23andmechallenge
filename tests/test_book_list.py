import pytest
from .context import BooksList
from .context import Book


class TestBookList:



    def test_should_be_empty_by_default(self):
        book_shelf = BooksList([])
        assert len(book_shelf) == 0

    def test_should_error_out_if_something_but_a_book_is_added(self):
        book_shelf = BooksList([])
        with pytest.raises(AssertionError):
            book_shelf.append("I am not a book for sure")

    def test_should_be_able_to_add_a_book(self):
        book_shelf = BooksList([])
        book_mock = Book()
        book_shelf.append(book_mock)
        assert len(book_shelf) == 1
        assert book_shelf[0] == book_mock
