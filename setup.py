from setuptools import setup

setup(
    name="23andMeBookSearch",
    version='0.1',
    licence='WTFPL',
    py_modules=['23am'],
    packages=['cli'],
    install_requires=[
        'Click',
        'urllib3',
        'python-dateutil',
        'pytest',
        'setuptools',
        'certifi'
    ],
    entry_points='''
        [console_scripts]
        bamcli=cli.main:main
    ''',
)