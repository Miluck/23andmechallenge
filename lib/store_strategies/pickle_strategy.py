import pickle
import os

'''
Strategy for storing objects the pickled fashion
'''


class PickleStrategy:
    def write(self, obj):
        pickle.dump(obj, open("{}.p".format(obj.name), "wb"))
        return True

    def read(self, name):
        return pickle.load(open("{}.p".format(name), "rb"))

    def remove(self, name):
        os.remove("{}.p".format(name))