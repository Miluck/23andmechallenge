from tests.context import BooksAndMeClient
from tests.context import BooksShelf
from tests.context import Book

class TestBooksAndMeClient:
    def test_can_be_initialized(self):
        client = BooksAndMeClient()
        assert isinstance(client, BooksAndMeClient)

    def test_can_create_a_shelf(self):
        client = BooksAndMeClient()
        shelf = client.create_shelf('foooShelf')
        assert isinstance(shelf, BooksShelf)

    def test_can_load_a_shelf(self):
        # TODO Mock
        shelf_name = 'foooShelf'
        client = BooksAndMeClient()
        client.create_shelf(shelf_name)
        shelf = client.get_shelf(shelf_name)
        shelf.append(Book())
        assert isinstance(shelf, BooksShelf)
        assert shelf.name == shelf_name
        assert len(shelf) == 1

    def can_search_for_books(self):
        client = BooksAndMeClient()
        client.get_books('foo')

