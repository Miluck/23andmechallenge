import pytest
import json
import os
import dateutil.parser
from .context import GoogleBookDecoder
from .context import Book


@pytest.fixture
def json_book():
    with open(os.path.join(os.path.dirname(__file__), 'fixtures/not_for_sale_book.json'), 'r') as f:
        return json.load(f)


@pytest.fixture
def sale_json_book():
    with open(os.path.join(os.path.dirname(__file__), 'fixtures/for_sale_book.json'), 'r') as f:
        return json.load(f)


class TestBookDecoder:
    def test_that_book_can_be_decoded_from_json(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert isinstance(book, Book)

    def test_that_book_title_title_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.title == json_book['volumeInfo']['title']

    def test_that_book_published_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.publisher == json_book['volumeInfo']['publisher']

    def test_that_book_description_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.description == json_book['volumeInfo']['description']

    def test_that_book_source_origin_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.source_origin == "Google"

    def test_that_book_source_id_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.source_id == json_book['id']

    def test_that_not_for_sale_book_price_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.price == 0

    def test_that_for_sale_book_price_is_decoded(self, sale_json_book):
        book = GoogleBookDecoder().decode(sale_json_book)
        assert book.price == sale_json_book['saleInfo']['listPrice']['amount']

    def test_that_book_avg_rating_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.avg_rating == json_book['volumeInfo']['averageRating']

    def test_that_book_rating_count_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.rating_count == json_book['volumeInfo']['ratingsCount']

    def test_that_book_published_date_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.published_date == dateutil.parser.parse(json_book['volumeInfo']['publishedDate']).date()

    def test_that_book_page_count_is_decoded(self, json_book):
        book = GoogleBookDecoder().decode(json_book)
        assert book.page_count == json_book['volumeInfo']['pageCount']
