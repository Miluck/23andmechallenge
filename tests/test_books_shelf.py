import pytest
from unittest.mock import MagicMock
from .context import BooksShelf
from .context import Book


class TestBooksShelf:
    def test_books_shelf_cannot_be_instantiated_with_elements(self):
        with pytest.raises(TypeError):
            BooksShelf([Book()])

    def test_books_shelf_can_be_saved_to_disk(self):
        strategy_mock = MagicMock()
        shelf = BooksShelf(strategy_mock, 'foo')
        shelf.append(Book())
        assert strategy_mock.save.assert_called
        assert len(shelf) == 1
        assert isinstance(shelf[0], Book)
