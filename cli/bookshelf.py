import click
from .bamcli import bamcli
from .controllers.books_controller import display_books
from .validators.sort_validator import validate as validate_sort


@bamcli.group()
def bookshelf():
    """Bookshelf operations"""
    pass


@bookshelf.command()
@click.pass_context
def drop(ctx):
    shelf_name = ctx.obj['current_shelf'].name
    ctx.obj['client'].delete_shelf(shelf_name)
    if click.confirm("Are you sure to delete the bookshelf?"):
        click.echo("The book shelf has been deleted.")


@bookshelf.command()
@click.pass_context
@click.option('--sort_by', callback=validate_sort, default='title', help='What the shelf should be sorted by.')
def describe(ctx, sort_by):
    shelf = ctx.obj['current_shelf']
    shelf.sort(key=lambda x: getattr(x, sort_by), reverse=True)
    display_books(click, shelf)


@bookshelf.command()
@click.pass_context
def summary(ctx):
    shelf = ctx.obj['current_shelf']
    click.echo(shelf.get_summary())
