import click
from .bamcli import bamcli
from .controllers.books_controller import search_books
from .controllers.bookshelf_controller import add_book


@bamcli.group()
def books():
    """Books operations"""
    pass


@books.command()
@click.pass_context
@click.argument('intent')
def search(ctx, intent):
    books = search_books(click, ctx.obj['client'], intent)
    ctx.obj['search_results'] = books
    shelf = ctx.obj['current_shelf']
    if len(books) == 0: return
    if click.confirm('Do you want to add any of the books to shelf?'):
        add_book(click, shelf, books)
