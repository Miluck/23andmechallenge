def search_books(click, client, intent):
    click.echo('Searching books for "{}"...'.format(intent))
    books = client.get_books(intent)
    if not books:
        click.echo("We could not find any results matching the search criteria.")
        return []
    display_books(click, books)
    return books


def display_books(click, books):
    click.clear()
    click.echo_via_pager(books)
