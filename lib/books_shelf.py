from .data.book_list import BooksList


class BooksShelf(BooksList):
    def __init__(self, store_strategy, name):
        list.__init__(self, [])
        self.store_strategy = store_strategy
        self.name = name
        self.save()

    def append(self, book):
        super().append(book)
        self.save()

    def get_summary(self):
        return("\nThe shelf has: '{}' books.".format(len(self)) +
            "\nTotal of '{}' pages.".format(sum(map(lambda book: book.page_count, self))))

    def save(self):
        self.store_strategy.write(self)
