"""
Book data class
"""
from datetime import date


class Book:
    def __init__(self):
        self.title = ""
        self.publisher = ""
        self.description = ""
        self.source_origin = ""
        self.source_id = ""
        self.price = 0
        self.avg_rating = 0
        self.rating_count = 0
        self.published_date = date(1, 1, 1)
        self.page_count = 0

    def __str__(self):
        # I wonder if there is a shorter way of doing it without looping over attributes
        str = ""
        template = "\n\n{}: {}"
        if self.title: str += template.format('Title: ', self.title)
        if self.publisher: str += template.format('Publisher: ', self.publisher)
        if self.description: str += template.format('Description: ', self.description)
        if self.price: str += template.format('Price: ', self.price)
        if self.avg_rating: str += template.format('Average Rating: ', self.avg_rating)
        if self.rating_count: str += template.format('Rating Count: ', self.rating_count)
        if self.published_date != date(1, 1, 1): str += template.format('Published Date: ', self.published_date)
        if self.page_count: str += template.format('Page Count: ', self.page_count)

        return str





        # return "\nTitle: {title}" \
        #        "\n Publisher: {publisher}" \
        #        "\n Description: {description}" \
        #        "\n Price: {price}" \
        #        "\n Average Rating: {avg_rating}" \
        #        "\n Published: {published_date}" \
        #        "\n Rating Count: {rating_count}" \
        #        "\n Page Count: {page_count}" \
        #     .format(
        #         title=self.title,
        #         publisher=self.publisher,
        #         price=self.price,
        #         avg_rating=self.avg_rating,
        #         published_date=self.published_date,
        #         description=self.description,
        #         rating_count=self.rating_count,
        #         page_count=self.page_count,
        #     )
