import json
import dateutil.parser
from lib.data.book import Book

"""
This is a decoder for Google Books -> app Book mapping
Don't really think is good code, but not sure how to refactor too
"""


class GoogleBookDecoder(json.JSONDecoder):
    def decode(self, obj):
        book = Book()
        volume_info = obj['volumeInfo']
        sales_info = obj['saleInfo']

        book.title = volume_info.get('title') or book.title
        book.publisher = volume_info.get('publisher') or book.publisher
        book.description = volume_info.get('description') or book.description
        book.source_origin = "Google"
        book.source_id = obj['id']
        if sales_info.get('listPrice'): book.price = sales_info['listPrice']['amount']
        book.avg_rating = volume_info.get('averageRating') or book.avg_rating
        book.rating_count = volume_info.get('ratingsCount') or book.rating_count
        if volume_info.get('publishedDate'): book.published_date = dateutil.parser.parse(volume_info['publishedDate']).date()
        book.page_count = volume_info.get('pageCount') or book.page_count

        return book
