from .book import Book


class BooksList(list):
	def __init__(self, *args):
		list.__init__(self, *args)

	def append(self, book):
		# TODO Read error message from config
		assert isinstance(book, Book), "You can only add books to the shelf."
		super().append(book)

	def __str__(self):
		representation = ""
		devider_size = 15
		devider = (devider_size * "=")
		for ind, book in enumerate(self):
			header = devider + " {} ".format(ind) + devider
			representation += "\n\n" + header + "\n" + \
							  str(book)
		return representation