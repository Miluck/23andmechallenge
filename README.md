##Command line application with the data that returned from the Google books API.

##### User will be able to add books from the search to his/her bookshelf as well as drop and explore the bookshelf.
##### Right now the implementation of bookshelf pickles the object to local disk (I've asked if I can use a DB and this question has never been answered unfortunately.)
##### If books are missing any attributes like price/description/etc.. the attribute is NOT going to be rendered on the screen.
##### Search is configured to be 20 results long, this is a usability decision
##### User will be able to add the same book to his/her bookshelf, it is known and intentional

#Requirements
`python 3` and
`Internet connection`

# Installation
`pip3 install --editable .`


# Running tests
`make test`

## Test coverage:
Core lib is covered on decent level
CLI is not automated, because Click framework guys have really screwed up to explain how to mock context
More research/time needed to cover cli with unit tests

# Usage
`bamcli` - stands for book and me command line interface
## Get help
`bamcli --help`

## Search for books
`bamcli books search '<intent>'`

## Show books in the book shelf
`bamcli bookhelf describe --sort_by=<sort_by>` if sort_by is not provided default 'title' will be used

## Drop books shelf
`bamcli bookshelf drop`

## Get shelf summary
`bamcli bookshelf summary`


# Tech. spec.
'Click' is being used as the cli framework http://click.pocoo.org/5/
The Application defines it's own schema for a book, so the application is source independent
(Can be extended to more sources, not just Google Books)








