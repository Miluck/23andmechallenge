import click
from unittest.mock import MagicMock
from click.testing import CliRunner
from ..context import search


class TestCommands:
    def test_books_serach(self):
        runner = CliRunner()
        intent = 'Java'

        google_mock = MagicMock()
        context_mock = MagicMock()
        context_mock.obj.return_value = {'client': google_mock}

        result = runner.invoke(search, [context_mock, intent])
        # TODO UGHUGHUGHUG
        # I hate that framework, could not find how to mock context
        # context_mock.get_books.assert_called()
        # assert result.exit_code == 0
        # assert 'Searching books for "{}"...'.format(intent) in result.output
        # assert result.output.count('Title: ') == 20
