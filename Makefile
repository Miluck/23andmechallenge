init:
	pip3 install -r requirements.txt

test:
	py.test -s tests

.PHONY: init test
