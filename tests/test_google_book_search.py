from unittest.mock import Mock
from unittest.mock import MagicMock
import os
import json
import urllib.parse
import pytest
from .context import GoogleBooksClient
from .context import Book
from .context import BooksList


@pytest.fixture
def books():
    with open(os.path.join(os.path.dirname(__file__), 'fixtures/books.json'), 'r') as f:
        return json.load(f)


class TestGoogleBookSearch:
    def test_can_search_for_books_if_no_result(self):
        intent = "Java"
        client_mock = Mock()
        search = GoogleBooksClient(client_mock)
        search._get_json_books = MagicMock(return_value=None)
        books = search.get_books(intent)

        client_mock.request.assert_called_once_with(
            'GET',
            'https://www.googleapis.com/books/v1/volumes?q={}&maxResults=20'.format(intent)
        )
        assert len(books) == 0
        assert isinstance(books, BooksList)

    def test_search_for_books_encodes_query_string(self):
        intent = "New York"
        query = urllib.parse.urlencode({'q': intent, 'maxResults': 20})
        expected_url = 'https://www.googleapis.com/books/v1/volumes?{}'.format(query)
        client_mock = Mock()
        search = GoogleBooksClient(client_mock)
        search._get_json_books = MagicMock(return_value=None)
        search.get_books(intent)
        client_mock.request.assert_called_once_with(
            'GET',
            expected_url
        )

    def test_can_search_for_books(self, books):
        intent = "java"

        client_mock = Mock()

        search = GoogleBooksClient(client_mock)
        search._get_json_books = MagicMock(return_value=books)

        books = search.get_books(intent)

        assert len(books) == 10
        assert isinstance(books, BooksList)
        assert isinstance(books[0], Book)
