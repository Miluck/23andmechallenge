from datetime import date
from .context import Book


class TestBook:
    def test_book_can_be_initialized(self):

        book = Book()
        assert book.title == ""
        assert book.publisher == ""
        assert book.description == ""
        assert book.source_origin == ""
        assert book.source_id == ""
        assert book.price == 0
        assert book.avg_rating == 0
        assert book.rating_count == 0
        assert book.published_date == date(1, 1, 1)
        assert book.page_count == 0
