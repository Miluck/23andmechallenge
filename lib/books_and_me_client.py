from .clients.google_books_client import GoogleBooksClient
from .books_shelf import BooksShelf
from .store_strategies.pickle_strategy import PickleStrategy

'''
The main application client
'''


class BooksAndMeClient:
    def __init__(self, client=None, store_strategy=None):
        self.client = client or GoogleBooksClient()
        self.store_strategy = store_strategy or PickleStrategy()
    '''
    Returns a BooksList object
    '''

    def get_books(self, intent):
        return self.client.get_books(intent)

    '''
    Creates a new shelf object
    Here store strategy is being specified
    '''

    def create_shelf(self, name):
        return BooksShelf(self.store_strategy, name)

    '''
    Get shelf by it's name
    '''

    def get_shelf(self, name):
        return self.store_strategy.read(name)

    def delete_shelf(self, name):
        self.store_strategy.remove(name)