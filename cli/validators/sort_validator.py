import click


def validate(ctx, param, value):
    options = ['title', 'price', 'avg_rating', 'published_date', 'page_count', 'rating_count']
    if value not in options:
        raise click.BadParameter('You cannot sort by "{}". Sorting options are: {}'.format(value, options))
    return value