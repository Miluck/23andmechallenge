def add_book(click, shelf, books):
    found_books = books

    for ind, book in enumerate(found_books):
        click.echo("BookNumber: {} | Title: {}".format(ind, book.title))

    if len(found_books) == 0:
        click.echo("Nothing to add yet. Please search for books first.")
        return

    index = click.prompt('\nWhich book would you like to add to your bookshelf?',
                         type=click.IntRange(0, len(found_books) - 1))

    chosen_book = found_books[index]

    if click.confirm("Are you sure you want to add '{}' book?".format(chosen_book.title)):
        shelf.append(chosen_book)