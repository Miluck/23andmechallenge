import os

import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from lib.data.book import Book
from lib.data.book_list import BooksList
from lib.books_shelf import BooksShelf
from lib.decoders.google_books.google_book_decoder import GoogleBookDecoder
from lib.clients.google_books_client import GoogleBooksClient
from lib.books_and_me_client import BooksAndMeClient
from cli.bamcli import bamcli
from cli.books import search