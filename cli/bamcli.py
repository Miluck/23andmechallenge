import click
from lib.books_and_me_client import BooksAndMeClient


@click.group()
@click.pass_context
def bamcli(ctx):
    shelf_name = 'mainShelf'
    client = BooksAndMeClient()
    ctx.obj['client'] = client
    ctx.obj['current_shelf'] = load_shelf(client, shelf_name)
    ctx.obj['search_results'] = []
    pass


"""
Ensure that shelf is loaded/created
"""


def load_shelf(client, shelf_name):
    try:
        return client.get_shelf(shelf_name)
    except FileNotFoundError:
        return client.create_shelf(shelf_name)
