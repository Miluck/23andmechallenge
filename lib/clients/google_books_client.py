import json
import certifi
import urllib3
import urllib.parse

from lib.data.book_list import BooksList
from lib.decoders.google_books.google_book_decoder import GoogleBookDecoder


class GoogleBooksClient:
    def __init__(self, client=None, limit=20):
        self.limit = limit
        self.client = client or urllib3.PoolManager(
            cert_reqs='CERT_REQUIRED',
            ca_certs=certifi.where()
        )

    def get_books(self, intent):
        # TODO move out to config
        query = urllib.parse.urlencode({'q': intent, 'maxResults': self.limit})
        url = "https://www.googleapis.com/books/v1/volumes?{}".format(query)
        resp = self.client.request('GET', url)
        json_books = self._get_json_books(resp)
        return self._parse_books(json_books)

    def _get_json_books(self, resp):
        return json.loads(resp.data.decode('utf-8')).get('items')

    def _parse_books(self, json_books):
        decoder = GoogleBookDecoder()
        books = BooksList([])
        if json_books is None: return books
        for book in json_books:
            books.append(decoder.decode(book))
        return books
